with states as (
    select 
        id, name
    from {{ source('stg_source', 'states') }}
)

select * from states