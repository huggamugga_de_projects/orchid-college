with enrollments as (
    select 
        id, course_id, student_id, created_at
    from {{ source('stg_source', 'enrollments') }}
)

select * from enrollments