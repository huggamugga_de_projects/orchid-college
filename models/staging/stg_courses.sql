with courses as (
    select 
        id, name
    from {{ source('stg_source', 'courses') }}
)

select * from courses