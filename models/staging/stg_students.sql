with students as (
    select 
        id, 
        name, 
        state_id, 
        gender, 
        created_at, 
        updated_at, 
        dbt_valid_from as valid_from, 
        dbt_valid_to as valid_to
    from {{ ref('students_snapshot') }}
)

select * from students