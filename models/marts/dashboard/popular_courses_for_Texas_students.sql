with students_currently_living_in_texas as (
    select id from {{ ref('dim_students') }}
    where valid_to is null and state_name = 'Texas'
),
enrollments as (
    select * from {{ ref('fact_enrollments') }}
),
courses as (
    select * from {{ ref('dim_courses') }}
)

select c.name, count(c.name)
from courses c
join enrollments e on e.course_id = c.id
join students_currently_living_in_texas sclit on sclit.id = e.student_id
group by c.name
order by count(c.name) desc
