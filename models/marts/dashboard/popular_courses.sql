with courses as (
    select id, name
    from {{ ref('dim_courses') }}
),
taken_courses as (
    select course_id
    from {{ ref('fact_enrollments') }}
)

select c.name, count(c.name)
from taken_courses tc
join courses c on tc.course_id = c.id
group by c.name
order by count(c.name) desc