with students as (
    select * from {{ ref('stg_students') }}
),
states as (
    select * from {{ ref('stg_states') }}
)

select s.id, s.name, st.name as state_name, s.gender, s.created_at, s.updated_at, s.valid_from, s.valid_to
from students s
join states st on s.state_id = st.id