{% snapshot students_snapshot %}

{{
    config(
      target_database='dbtdb',
      target_schema='orchid_college_schema',
      unique_key='id',

      strategy='timestamp',
      updated_at='updated_at',
    )
}}

select * from {{ source('orchid_college', 'students') }}

{% endsnapshot %}