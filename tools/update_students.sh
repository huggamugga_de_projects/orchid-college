# $1: container name
# $2: database name

docker exec -it $1 psql -U postgres $2 -a -f orchid_college/sql_commands/update_students.sql