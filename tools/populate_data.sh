# $1: container name
# $2: database name

docker exec -it $1 psql -U postgres $2 -a -f orchid_college/sql_commands/create_tables.sql
docker exec -it $1 psql -U postgres $2 -a -f orchid_college/sql_commands/populate_college_data.sql