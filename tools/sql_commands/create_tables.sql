BEGIN;

CREATE SCHEMA IF NOT EXISTS orchid_college_schema;

CREATE TABLE IF NOT EXISTS orchid_college_schema.states (
    id VARCHAR(4),
    name TEXT
);

CREATE TABLE IF NOT EXISTS orchid_college_schema.courses (
    id VARCHAR(4),
    name TEXT
);

CREATE TABLE IF NOT EXISTS orchid_college_schema.students (
    id VARCHAR(10),
    name TEXT,
    state_id VARCHAR(4),
    gender TEXT,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS orchid_college_schema.enrollments (
    id VARCHAR(10),
    course_id VARCHAR(4),
    student_id VARCHAR(10),
    created_at timestamp with time zone
);

COMMIT;