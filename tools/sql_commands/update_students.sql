BEGIN;

CREATE TABLE IF NOT EXISTS orchid_college_schema.students_temp (
    id VARCHAR(10),
    name TEXT,
    state_id VARCHAR(4),
    gender TEXT,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);

COPY orchid_college_schema.students_temp
FROM '/orchid_college/students_new.csv'
DELIMITER ','
CSV HEADER;

UPDATE orchid_college_schema.students s
SET id = st.id, name = st.name, state_id = st.state_id, gender = st.gender, created_at = st.created_at, updated_at = st.updated_at
FROM orchid_college_schema.students_temp as st
WHERE s.id = st.id;

DROP TABLE IF EXISTS orchid_college_schema.students_temp;

COMMIT;