BEGIN;

COPY orchid_college_schema.states
FROM '/orchid_college/states.csv'
DELIMITER ','
CSV HEADER;

COPY orchid_college_schema.courses
FROM '/orchid_college/courses.csv'
DELIMITER ','
CSV HEADER;

COPY orchid_college_schema.students
FROM '/orchid_college/students.csv'
DELIMITER ','
CSV HEADER;

COPY orchid_college_schema.enrollments
FROM '/orchid_college/enrollments.csv'
DELIMITER ','
CSV HEADER;

COMMIT;